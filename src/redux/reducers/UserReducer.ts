import Types from '../types';

type actionType = {
  type: string;
  payload: any;
};

const initState = {
  create_date: '',
  id: 0,
  login: '',
};

const UserReducers = (state = initState, action: actionType) => {
  const {type, payload} = action;
  switch (type) {
    case Types.LOGIN_SUCCESS:
      return {
        ...state,
        user: payload,
      };
    case Types.CLEAR_USER:
      return initState;
    default:
      return state;
  }
};

export default UserReducers;
