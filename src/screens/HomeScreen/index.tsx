import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import styles from './style';
import APi from '../../api/user';
interface Props {
  navigation: any;
}
const HomeScreen = (props: Props) => {
  const {navigation} = props;
  const [text, setText] = useState(0);
  // Giống vơi componentDidMount
  useEffect(() => {
    _initData();
  }, []);

  const _goBack = () => {
    navigation.goBack();
  };

  const _add = () => {
    setText(text + 1);
  };
  // get API
  const _initData = async () => {
    const params = {
      phone_number: '',
      token: '',
      device_id: '',
    };

    try {
      const res = await APi.loginPhone(params);
    } catch (error) {
      console.log('error', error);
    }
  };

  return (
    <View style={styles.container}>
      <Text>{text}</Text>
      <Text>HomeScreen</Text>
      <Text onPress={_goBack}>Back</Text>
      <Text onPress={_add}>Add One</Text>
    </View>
  );
};
export default HomeScreen;
