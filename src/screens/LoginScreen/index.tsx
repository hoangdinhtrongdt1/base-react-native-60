import React from 'react';
import {View, Text} from 'react-native';
import styles from './style';
interface Props {
  navigation: any;
}
const LoginScreen = (props: Props) => {
  const {navigation} = props;

  const _onPressLogin = () => {
    navigation.navigate('AppStack');
  };

  return (
    <View style={styles.container}>
      <Text onPress={_onPressLogin}>Login</Text>
    </View>
  );
};
export default LoginScreen;
